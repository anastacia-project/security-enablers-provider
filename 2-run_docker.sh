#!/bin/bash

 USAGE=" USAGE: ./2-run_docker.sh security_enabler_provider 8001:8001"

if [ $# -eq 0 ]
  then
    echo $USAGE
    exit 1
fi

#sudo docker run --name $1 -d -p $2 $1
docker rm $1
docker run -e RL_ADDR="10.79.7.176" -e RL_PORT=9092 --name $1 -p $2 -v $(pwd):/app -i -t $1
