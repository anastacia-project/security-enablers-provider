# # -*- coding: utf-8 -*-
# policy_interpreter.py
"""
This python module implements a fast protopype of the security enabler provider.
How to use:
	Run the server: gunicorn security_enabler_provider:app --reload -b 0.0.0.0:8000
	Request plugin list by capabilities: curl localhost:8000/get_plugins?capabilities='c1','c2' 
        i.e.: curl localhost:8001/get_plugins?capabilities=Filtering_L4
	Request plugin by name: curl localhost:8000/get_plugin?name=[PLUGIN_NAME]
        i.e.: 
"""
__author__ = "Alejandro Molina Zarca"
__copyright__ = "Copyright 2018, ANASTACIA H2020"
__credits__ = ["Antonio Skarmeta", "Jorge Bernal Bernabé", "Alejandro Molina Zarca"]
__license__ = "GPL"
__version__ = "0.0.1"
__maintainer__ = "Alejandro Molina Zarca"
__email__ = "alejandro.mzarca@um.es"
__status__ = "Development"

# Let's get this party started!
import falcon
import json
import logging
from settings import logging, remote_logger
from datetime import datetime
logger = logging.getLogger(__name__)

# Falcon follows the REST architectural style, meaning (among
# other things) that you think in terms of resources and state
# transitions, which map to HTTP verbs.
class PluginCatalog(object):

    PLUGIN_CAPABILITIES = {
                            "onos_nb": set(["Filtering_L4","Traffic_Divert"]),
                            "iptables": set(["Filtering_L4"]),
                            "ovs-fw": set(["Filtering_L4"]),
                            "odl": set(["Filtering_L4"]),
                            "cooja": set(["IoT_honeynet"]),
                            "paa": set(["Authentication"]),
                            "iot_controller": set(["IoT_control","Protection_confidentiality"]),
                            "xacml": set(["AuthoriseAccess_resurce"]),
                            "dtls_proxy": set(["Protection_confidentiality"]),
                            "dtls_server": set(["Protection_confidentiality"]),
                            "mmt_agent": set(["Network_traffic_analysis"]),
                            "utrc_agent": set(["Data_analysis"]),
                            "cpabe_proxy": set(["Privacy"]),
                            "slicing_agent": set(["Network_slicing"]),
                            }

    def on_get(self, req, resp):
        """Handles GET requests"""
        # TODO: short the code
        candidates = []
        requested_capabilities = set(req.get_param_as_list("capabilities",required=True))

        logging_message = {
         "timestamp": datetime.timestamp(datetime.now()),
         "from_module": "POLICY_MANAGER",
         "from_component": "POLICY_INTERPRETER",
         "to_module": "SECURITY_ENABLERS_PROVIDER",
         "to_component": "SECURITY_ENABLERS_PROVIDER",
         "incoming": True,
         "method": "REST",
         "data": repr(requested_capabilities),
         "notes": "Enablers List request"
        }
        remote_logger.info(logging_message)

        #logger.debug("Searching Security Enablers for {} capabilities".format(requested_capabilities))
        for security_enabler_name, capability_enabler_set in self.PLUGIN_CAPABILITIES.items():
            ##logger.debug("comparing {} with {}".format(capability_enabler_set,requested_capabilities))
            #if not capability_enabler_set.symmetric_difference(requested_capabilities):
            if requested_capabilities.issubset(capability_enabler_set):
                candidates.append(security_enabler_name)
        if not candidates:
            logger.info("NOT CANDIDATES AVAILABLE")
            raise falcon.HTTPBadRequest(
                "Enabler not available",
                "There is no enabler for the provided capabilities"
            ) 

        resp.status = falcon.HTTP_200  # This is the default status
        resp.body = (json.dumps({"candidate_security_enablers":candidates}))

        logging_message = {
         "timestamp": datetime.timestamp(datetime.now()),
         "from_module": "SECURITY_ENABLERS_PROVIDER",
         "from_component": "SECURITY_ENABLERS_PROVIDER",
         "to_module": "POLICY_MANAGER",
         "to_component": "POLICY_INTERPRETER",
         "incoming": False,
         "method": "REST",
         "data": resp.body,
         "notes": "Enablers List result"
        }
        remote_logger.info(logging_message)
        #print("Selected Security Enablers: {}".format(candidates))

class Plugin(object):
    def on_get(self, req, resp):
        """Handles GET requests"""
        security_enabler_name = req.get_param("name",required=True)
        #logger.debug("Searching a plugin for '{}' security enabler{}".format(security_enabler_name,"."*3))
        plugin_file_name = "mspl_{}.py".format(security_enabler_name)
        plugin_path = "m2l_plugins/{}".format(plugin_file_name)

        logging_message = {
         "timestamp": datetime.timestamp(datetime.now()),
         "from_module": "POLICY_MANAGER",
         "from_component": "POLICY_INTERPRETER",
         "to_module": "SECURITY_ENABLERS_PROVIDER",
         "to_component": "SECURITY_ENABLERS_PROVIDER",
         "incoming": True,
         "method": "REST",
         "data": security_enabler_name,
         "notes": "Enabler Plugin request"
        }
        remote_logger.info(logging_message)

        try:
            plugin = open(plugin_path,"r")
            resp.body = (json.dumps({"plugin_file_name":plugin_file_name,"plugin":plugin.read()}))

            logging_message = {
             "timestamp": datetime.timestamp(datetime.now()),
             "from_module": "SECURITY_ENABLERS_PROVIDER",
             "from_component": "SECURITY_ENABLERS_PROVIDER",
             "to_module": "POLICY_MANAGER",
             "to_component": "POLICY_INTERPRETER",
             "incoming": False,
             "method": "REST",
             "data": resp.body,
             "notes": "Enablers Plugin result"
            }
            remote_logger.info(logging_message)

            #logger.debug("'{}' Security Enabler founded!. '{}' file was sent".format(security_enabler_name,plugin_file_name))
        except (FileNotFoundError,IOError) as e:
            resp.status = falcon.HTTP_500
            resp.body = str(e)
            return


class HealthService(object):

    def on_get(self, req, resp):
        resp.body = json.dumps({"status":"OK"})


# falcon.API instances are callable WSGI apps
app = falcon.API()

# Resources are represented by long-lived class instances
plugin_catalog = PluginCatalog()
plugin = Plugin()
health_service = HealthService()

# things will handle all requests to the '/things' URL path
app.add_route('/get_plugins', plugin_catalog)
app.add_route('/get_plugin', plugin)
app.add_route('/health', health_service)