import os
from logger.handlers import KafkaHandler
import logging
logging.basicConfig(level=logging.INFO)

# remote logging
remote_logger = logging.getLogger("remote_logger")
remote_logger.setLevel(logging.INFO)
remote_logger.propagate = False
# Set remote logging handler
#REMOTE_LOGGING_URL = ['172.17.0.1:9092']
REMOTE_LOGGING_URL = ["{}:{}".format(os.environ["RL_ADDR"],os.environ["RL_PORT"])]
TOPIC = 'anastacia-log'
try:
	kh = KafkaHandler(REMOTE_LOGGING_URL, TOPIC)
	remote_logger.addHandler(kh)
except Exception as e:
	logging.info(e)