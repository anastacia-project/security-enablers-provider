# # -*- coding: utf-8 -*-
"""
This python module Is a copy of the dtls_proxy just for testing purposes.
How to test:
	python3 mspl_dtls_proxy.py test/[MSPL_FILE.xml]

"""
__author__ = "Alejandro Molina Zarca"
__copyright__ = "Copyright 2018, ANASTACIA H2020"
__credits__ = ["Antonio Skarmeta", "Jorge Bernal Bernabé", "Alejandro Molina Zarca"]
__license__ = "GPL"
__version__ = "0.0.1"
__maintainer__ = "Alejandro Molina Zarca"
__email__ = "alejandro.mzarca@um.es"
__status__ = "Development"


import sys,os
import json
import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)
# Insert the parent path into the sys.path in order to import the mspl
parentPath = os.path.abspath("..")
if parentPath not in sys.path:
    sys.path.insert(0, parentPath)
import mspl

"""
	Output example:
	target=[TARGET]&method=[METHOD]&resource=[RESOURCE]&payload=[PAYLOAD]
	{
        “target”: “aaaa::2”,
        "port": "5683",
        “method”: “PUT”,
        “resource”:”.reset”
        “payload”: 1
    }
"""

class ITResourceType(mspl.ITResourceType):

	def get_configuration(self):
		'Translate the ITResource element to IoT Controller Northbound configuration'
		capability_name = self.configuration.capability[0].Name
		if not capability_name in mspl.CapabilityType.itervalues():
			raise ValueError('The capability {} does not exist, please try with the current supported capabilities'.format(capability_name))
		# In filtering case, call translation for filtering Action and filtering conf condition
		try:
			capability_configuration_function = getattr(self, "get_{}_configuration".format(capability_name))
		except AttributeError as e:
			print("Currently, {} is not implemented.".format(capability_name))
			print(e)
			sys.exit(1)

		enabler_configuration = capability_configuration_function()
		# Build the iot controller northbound parameters
		#enabler_conf = "target={}&method={}&resource={}&payload={}".format(enabler_configuration["target"], enabler_configuration["method"], enabler_configuration["resource"], enabler_configuration["payload"])
		return json.dumps(enabler_configuration)


	
	def get_DTLS_protocol_configuration(self):
		'Returns the configuration for DTLS_protocol capability'
		try:
			configuration_rule = self.configuration.configurationRule[0]
		except:
			print("DTLS_protocol capability requires at least a configurationRule element")
			sys.exit(1)
		return configuration_rule.configurationRuleAction.get_configuration()
		#target,port,resource,method = configuration_rule.configurationCondition.get_configuration()

		#enabler_configuration = {'target':target,'port':port,'method':method,'resource':resource,'payload':payload}

		#return enabler_configuration

class DataProtectionAction(mspl.DataProtectionAction):
	def get_configuration(self):
		'Returns the DTLS protection action for IoT Controller'
		technology_action_parameters = self.technologyActionParameters
		technology_parameter = technology_action_parameters.technologyParameter[0]
		#authentication_parameter = technology_action_parameters.authenticationParameters
		technology_action_security_property = self.technologyActionSecurityProperty
		# Get the iot-device
		taget = technology_parameter.localEndpoint
		# Get the payload information
		target = technology_parameter.localEndpoint
		dtls_proxy = technology_parameter.remoteEndpoint
		#ps_key_value = authentication_parameter.psKey_value
		enc_algorithm = technology_action_security_property[0].encryptionAlgorithm
		key_size = technology_action_security_property[0].keySize
		mode = technology_action_security_property[0].mode
		integrity_algorithm = technology_action_security_property[1].integrityAlgorithm

		enabler_configuration = {'target':target,'dtls_proxy':dtls_proxy,
								'pemk':'[PEMK]','key_size':key_size,
								'mode':mode,'integrity_algorithm':integrity_algorithm,
								"enc_algorithm":enc_algorithm}

		return enabler_configuration


class M2LPlugin:
	'IoT Controller Medium to low plugin implementation'

	def get_configuration(self,mspl_source):
		'Return the IoT Controller configuration from the mspl_source'
		# Customize the pyxb generated classes with our own behavior
		global mspl
		mspl.ITResourceType._SetSupersedingClass(ITResourceType)
		mspl.DataProtectionAction._SetSupersedingClass(DataProtectionAction)
		#Load the xml
		it_resource = mspl.CreateFromDocument(mspl_source)
		# Start the parser process using the xml root element
		return it_resource.get_configuration()

if __name__ == "__main__":
	xml_file = sys.argv[1] 
	m2lplugin = M2LPlugin()
	logger.info("Reading mspl file...")
	xml_source = open(xml_file).read()
	enabler_configuration = m2lplugin.get_configuration(xml_source)
	# Pretty print
	separator = "*"
	separator_group = separator*5
	title = "{} CONFIGURATION".format(sys.argv[0].split("mspl_")[1][:-3].upper())
	print("\n{}{}{}".format(separator_group,title,separator_group))
	print(enabler_configuration)
	print("{}{}{}\n".format(separator_group,separator*len(title),separator_group))