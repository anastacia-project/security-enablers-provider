# # -*- coding: utf-8 -*-
"""
This python module implements a MSPL->ONOS Northbound API plugin inside the ANASTACIA European Project,
extending the MSPL language defined in secured project.
How to use:
	python3 mspl_odl.py [MSPL_FILE.xml]

"""
__author__ = "Alejandro Molina Zarca"
__copyright__ = "Copyright 2018, ANASTACIA H2020"
__credits__ = ["Antonio Skarmeta", "Jorge Bernal Bernabé", "Alejandro Molina Zarca"]
__license__ = "GPL"
__version__ = "0.0.1"
__maintainer__ = "Alejandro Molina Zarca"
__email__ = "alejandro.mzarca@um.es"
__status__ = "Development"


import sys,os
import json
# Insert the parent path into the sys.path in order to import the mspl
parentPath = os.path.abspath("..")
if parentPath not in sys.path:
    sys.path.insert(0, parentPath)
import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

if "mspl" not in sys.modules:
	import mspl 
else:
	mspl = sys.modules["mspl"]


"""
	Filtering Output example:

	{
	  "priority": 60000,
	  "timeout": 0,
	  "isPermanent": true,
	  "deviceId": "of:000008002768a30a",
	  "treatment": {
	    "instructions": [
	      {
	        "type": "NOACTION"
	      }
	    ]
	  },
	  "selector": {
	    "criteria": [
	      {
	        "type": "ETH_TYPE",
	        "ethType": "0x86dd"
	      },
	      {
	        "type": "IPV6_SRC",
	        "ip": "aaaa::212:7402:2:202/64"
	      }
	    ]
	  }
	}

	Forwarding Example
	TODO

	ONOS NB API:
	https://wiki.onosproject.org/display/ONOS/Flow+Rules
	http://[CNTROLLER_IP]:8181/onos/v1/docs/#!/docs/get_docs_index_html

"""

class ITResourceType(mspl.ITResourceType):


	def fill_restconf_template(self,enabler_configuration):
		
		# loads and dumps in order to armonize the tabs
		return json.dumps(json.loads('''
  			{{
				"priority": {},
				"tableId": 0,
				"timeout": 0,
				"isPermanent": true,
				"deviceId": "[DEVICE_ID (e.g. of:000008002768a30a)]",
				"treatment": {{
				    "instructions": [
				      	{}
				    ]
				}},
				"selector": {{
				    "criteria": [
				      {{
				        "type": "ETH_TYPE",
				        "ethType": "0x86dd"
				      }},
				      	{}
				    ]
				}}
			}}
			'''.format(enabler_configuration["priority"],enabler_configuration["action"],enabler_configuration["filtering_enabler_conf"])),indent=4)

	def get_configuration(self):
		'Translate the ITResource element to ONOS Northbound API configuration'
		capability_name = self.configuration.capability[0].Name
		if not capability_name in mspl.CapabilityType.itervalues():
			raise ValueError('The capability {} does not exist, please try with the current supported capabilities'.format(capability_name))
		# In filtering case, call translation for filtering Action and filtering conf condition
		try:
			capability_configuration_function = getattr(self, "get_{}_configuration".format(capability_name))
		except AttributeError as e:
			print("Currently, {} is not implemented.".format(capability_name))
			print(e)
			sys.exit(1)

		enabler_configuration = capability_configuration_function()
		return enabler_configuration


	def get_Filtering_L4_configuration(self):
		'Returns the configuration for Filtering_L4 capability'
		# For filtering it is only needed the first configuration rule
		try:
			configuration_rule = self.configuration.configurationRule[0]
		except:
			print("Filtering_L4 capability requires at least a configurationRule element")
			sys.exit(1)
		action = configuration_rule.configurationRuleAction.get_configuration()
		filtering_enabler_conf = configuration_rule.configurationCondition.get_configuration()
		name = configuration_rule.Name
		priority = configuration_rule.externalData.get_configuration()

		enabler_configuration = {'action':action,'filtering_enabler_conf':filtering_enabler_conf,'name':name,'priority':priority}


		print("enabler_configuration:",enabler_configuration)

		return enabler_configuration

	def get_Privacy_configuration(self):
		try:
			configuration_rule = self.configuration.configurationRule[0]
		except:
			print("Privacy capability requires at least a configurationRule element")
			sys.exit(1)
		# Get the action
		action = configuration_rule.configurationRuleAction.get_configuration()
		# Get the criteria
		privacy_conf_condition = configuration_rule.configurationCondition.get_configuration()
		#name = configuration_rule.Name
		#priority = configuration_rule.externalData.get_configuration()


		#enabler_configuration = {'action':action,'filtering_enabler_conf':traffic_divert_enabler_conf,'name':name,'priority':priority}

		#print("enabler_configuration:",enabler_configuration)
		
		return {**action,**privacy_conf_condition}

class PrivacyAction(mspl.PrivacyAction):
	def get_configuration(self):
		'Returns the privacy action for CPABE Proxy Northbound API'
		action_type = self.PrivacyActionType
		print(self.PrivacyMethod)
		logger.debug(self.PrivacyMethod)
		logger.info(self.PrivacyMethod)
		attributes = self.PrivacyMethod.attribute
		attrs = []
		for attr in attributes:
			attrs.append("{}-{}".format(attr.key,attr.value_))

		# action type
		# attributes

		return {"CP-ABE":" ".join(attrs)}


class PrivacyConfigurationCondition(mspl.PrivacyConfigurationCondition):
	def get_configuration(self):
		'Returns the traslated filtering ONOS Northbound API parameters'
		privacy_conf_condition = {}
		packet_filter_condition = self.packetFilterCondition
		if not packet_filter_condition:
			return None

		#TODO: Verify ipv4 or ipv6
		ip_version = "6"
		privacy_conf_condition["Source"] = packet_filter_condition.SourceAddress if packet_filter_condition.SourceAddress else ""
		privacy_conf_condition["Destination"] = packet_filter_condition.DestinationAddress if packet_filter_condition.DestinationAddress else ""
		privacy_conf_condition["DestinationPort"] = packet_filter_condition.DestinationPort if packet_filter_condition.DestinationPort else ""
		privacy_conf_condition["Protocol"] = packet_filter_condition.ProtocolType if packet_filter_condition.ProtocolType else ""


		applicationLayerCondition = self.applicationLayerCondition
		privacy_conf_condition["Action"] = applicationLayerCondition.method if applicationLayerCondition.method else ""
		privacy_conf_condition["Resource"] = applicationLayerCondition.URL if applicationLayerCondition.URL else ""


		return privacy_conf_condition


class TrafficDivertConfigurationCondition(mspl.TrafficDivertConfigurationCondition):
	def get_configuration(self):
		# We cannot use super because the inheritance implementation of pyxb
		return FilteringConfigurationCondition.get_configuration(self)

class TrafficDivertAction(mspl.TrafficDivertAction):
	def get_configuration(self):
		'Returns the filtering action for ONOS Northbound API'
		FORWARD = "FORWARD"
		# Get the destination
		destination_address = self.packetDivertAction.packetFilterCondition.DestinationAddress
		interface = self.packetDivertAction.packetFilterCondition.Interface

		output = interface if interface else destination_address
		#destination = self.packetDivertAction.packetFilterCondition.DestinationAddress
		#destination = self.packetDivertAction.packetFilterCondition.DestinationAddress
		fwd = {"type": "OUTPUT",
        			"port": output}
		TRAFFIC_DIVERT_ACTION = {"FORWARD": fwd}
		
		return json.dumps(TRAFFIC_DIVERT_ACTION.get(self.TrafficDivertActionType,FORWARD))



class M2LPlugin:
	'ONOS Northbound API Medium to low plugin implementation'

	def get_configuration(self,mspl_source):
		'Return the ONOS Northbound API configuration from the mspl_source'
		# Customize the pyxb generated classes with our own behavior
		
		global mspl
		mspl.ITResourceType._SetSupersedingClass(ITResourceType)
		mspl.PrivacyAction._SetSupersedingClass(PrivacyAction)
		
		mspl.PrivacyConfigurationCondition._SetSupersedingClass(PrivacyConfigurationCondition)
		
		#Load the xml
		it_resource = mspl.CreateFromDocument(mspl_source)
		# Start the parser process using the xml root element
		return it_resource.get_configuration()
		
if __name__ == "__main__":
	xml_file = sys.argv[1] 
	m2lplugin = M2LPlugin()
	print("Reading mspl file...")
	xml_source = open(xml_file).read()
	enabler_configuration = m2lplugin.get_configuration(xml_source)
	# Pretty print
	separator = "*"
	separator_group = separator*5
	title = "{} CONFIGURATION".format(sys.argv[0].split("mspl_")[1][:-3].upper())
	print("\n{}{}{}".format(separator_group,title,separator_group))
	print(enabler_configuration)
	print("{}{}{}\n".format(separator_group,separator*len(title),separator_group))