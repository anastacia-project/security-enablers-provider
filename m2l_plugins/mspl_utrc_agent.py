# # -*- coding: utf-8 -*-
"""
This python module implements a MSPL->IPTABLES plugin inside the ANASTACIA European Project,
extending the MSPL language defined in secured project.
How to use:
	python3 mspl_iptables.py [MSPL_FILE.xml]

"""
__author__ = "Alejandro Molina Zarca, Piotr Sobonski"
__copyright__ = "Copyright 2018, ANASTACIA H2020"
__credits__ = ["Antonio Skarmeta", "Jorge Bernal Bernabé", "Alejandro Molina Zarca"]
__license__ = "GPL"
__version__ = "0.0.1"
__maintainer__ = "Alejandro Molina Zarca"
__email__ = "alejandro.mzarca@um.es"
__status__ = "Development"

import sys
import json
import xmltodict
import xml.etree.ElementTree as ET


import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

class M2LPlugin:
    'PAA Medium to low plugin implementation'
    def __convert_mspl(self, policy: list) -> None:
        """
        Method converts XML converted to JSON policy to
        UTRC Data Analysis Agent configuration
        Args:
            policy  - list of XML elements
        Returns:
            <dict>  - new agent configuration
        """
        config = {}

        #logger.info(policy)

        for elm in policy:
            
            #logger.info(elm[])
            #print("Parsing element: %s" % str(elm["configuration"]
            #                                  ["capability"]
            #                                  ["Name"]))
            # There might be XML/JSON misalignment protect this section against
            # any new MSPL/XML updates
            try:                
                logger.debug("key:{}".format(elm))
                logger.debug(policy[elm])
                logger.debug(policy[elm]["capability"]["Name"])
                if policy[elm]["capability"]["Name"] == "Data_analysis":
                    sub_el = policy[elm]\
                                ["configurationRule"]\
                                ["configurationCondition"]

                    # Parsing sensor name
                    sensor_name = "/" + sub_el["monitoringConfigurationCondition"]\
                                              ["packetFilterCondition"]\
                                              ["SourceAddress"][:-4]

                    # Parsing sensor verdict generation
                    sensor_state = sub_el["monitoringConfigurationCondition"]\
                                         ["packetFilterCondition"]\
                                         ["State"]
                    sensor_state = 1 if sensor_state.upper() == "ENABLED" else 0

                    config[sensor_name] = sensor_state
            except Exception as ex:
                logger.debug(ex)
        return config

    def get_configuration(self, mspl_source: str) -> dict:
        'Return the PAA configuration from the mspl_source'
        # print("Parsing %s" % mspl_source)
        config = {}
        #policy = xmltodict.parse(mspl_source)["ITResourceOrchestration"]["ITResource"]
        policy = xmltodict.parse(mspl_source)["ITResource"]
        # print(json.dumps(policy, indent=4))
        config = self.__convert_mspl(policy)
        print("Parsed config:")
        print(json.dumps(config, indent=4))

        return config

if __name__ == "__main__":
    xml_file = sys.argv[1] 
    m2lplugin = M2LPlugin()
    print("Reading mspl file...")

    # Correct XML file content encoding
    tree = ET.parse(xml_file)
    xml_data = tree.getroot()
    xml_source = ET.tostring(xml_data, encoding='utf-8', method='xml')

    enabler_configuration = m2lplugin.get_configuration(xml_source)

    # Pretty print
    separator = "*"
    separator_group = separator*5
    title = "{} CONFIGURATION".format(sys.argv[0].split("mspl_")[1][:-3].upper())
    print("\n{}{}{}".format(separator_group, title, separator_group))
    print(enabler_configuration)
    print("{}{}{}\n".format(separator_group, separator*len(title), separator_group))
