# # -*- coding: utf-8 -*-
"""
This python module implements a MSPL->OVS-fw plugin inside the ANASTACIA European Project,
extending the MSPL language defined in secured project.
How to use:
    python3 mspl_mmt.py [MSPL_FILE.xml]

"""
__author__ = "Huu-Nghia NGUYEN"
__copyright__ = "Copyright 2018, ANASTACIA H2020"
__credits__ = ["Antonio Skarmeta", "Jorge Bernal Bernabé", "Alejandro Molina Zarca", "Abderrahmane Boudi"]
__license__ = "GPL"
__version__ = "0.0.1"
__maintainer__ = "Huu-Nghia Nguyen"
__email__ = "huunghia.nguyen@montimage.com"
__status__ = "Development"


MMT_CONF = """
# This is the configuration file for MMT Probe

# the unique identifier given to the probe
#probe-id-number = 3
{{probe-id-number}}

# in case for PCAP the input mode can be online or offline, however for DPDK its only online
input-mode   = "online"
# folder where the license key exists
license_file_path = "/opt/mmt/probe/bin/license.key"

# indicates the file name where the log messages will be written to
logfile = "/opt/mmt/probe/log/online/log.data"    #default is "log.data" in the running folder

# input source for PCAP online mode (interface name) and for offline mode (pcap name), however for DPDK its interface port number
input-source = "ens4"
#{{input-source}}

# Indicates where the traffic reports will be dumped ( CSV file )
file-output
{
    enable = 1   # set to zero to disable file output ( CSV ) , 1 to enable
    data-file   = "dataoutput.csv"      # file name where the reports will be dumped :
    location = "/opt/mmt/probe/result/report/online/" # Location where files are written :
    #{{file-output.data-file}}
    #{{file-output.location}}
    retain-files  = 4   # retains the last x sampled files,
                         # set to 0 to retain all files ( note that the value of retain-files must be greater than the value of thread_nb + 1) :
    sampled_report = 0 # Set to 1 if one needs a sampled file every x seconds or
                       # 0 if one needs a single file :
    #output data type: either JSON or CSV
    format=CSV
}

# indicates REDIS output :
redis-output
{
    enabled = 0 # set to 0 to disable publishing to REDIS , 1 to enable
                # publishing to REDIS :
    hostname = "localhost" # hostname of REDIS server :
    port = 6379 # port number of REDIS server :
}

# indicates KAFKA output :
kafka-output
{
    enabled = 1 # set to zero to disable publishing to KAFKA , 1 to enable
                # publishing to KAFKA :
    #hostname = "10.79.7.178" # hostname of KAFKA server :
    #port = 9092 # port number of KAFKA server :
    {{kafka-output.hostname}}
    {{kafka-output.port}}
}

# All the sections below are optional and can be enable or disable according to the requirements.
#thread configuration (default 1 thread), stats-period (default 5 seconds), file-output-period (default 5 seconds)

# indicates the threads configuration options

thread-nb    = 1    # this is the number of threads MMT will use for processing packets. Must be positive.
thread-queue = 3000 # this is the maximum number of packets queued for processing that a single thread accepts (only for PCAP).
            # 0 means MMT decides how many packets to queue (default =1000)
            # If a packet is dispatched for a thread with a full queue the packet will be dropped and reported in MMT statistics
thread-data  = 0    # this is the maximum amount of data queued for processing that a single thread accepts (only for PCAP.
            # 0 means unlimited (will always be limited by the system memory)
            # If a packet is dispatched for a thread with full data the packet will be dropped and reported in MMT statistics

snap-len = 0        # 0 means default value 65535, apparently what tcpdump uses for -s 0 (only for PCAP)


# Indicates where the traffic reports will be dumped ( CSV file )
dump-session
{
    enable = 0   # set to zero to disable dumping packet
    location = "/opt/mmt/probe/pcaps/" # Location where files will be located
    protocols = {"unknowns"} # List of protocols which will be dumped
    time = 0 # New files (pcap) are created every time seconds. 0 means default value 3600 seconds

}

#indicates mmt_security input and output (only with PCAP)
security1
{
    enable  = 0   # set to 1 to perform security analysis , 0 to disable it :
    results-dir  = "/opt/mmt/probe/result/security/online/"   # folder where the detected breaches will be reported :
    properties-file  = "test_files/properties_all.xml"  # file containing security properties and rules
    output-channel = {} # Output reports to the channel, output-channel (string) can be redis, kafka, file. default: redis
                        # More than one output-channel is possible, if so inputs should be comma-separated value for example: {redis, kafka, files)
                # Output reports are send to channels only when the global parameters are enable (file-output.enable =1, redis-output.enable =1
                # kafka-output.enable =1 )

}

#smp-security
security2
{
    enable        = 1    # set to 1 to enable, 0 to disable it
    thread-nb     = 0    # the number of security threads per one probe thread , e .g . , if we have 16 probe threads and thread - nb = x ,
                # then x *16 security threads will be used .
                # If set to zero this means that the security analysis will be done in the same threads used by the probe .
    #exclude-rules = ""     # Range of rules to be excluded from the verification
    {{security.exclude-rules}}
    rules-mask    = ""    # Mapping of rules to the security threads:
                            # Format: rules-mask = (thread-index:rule-range);
                            # thread-index = a number greater than 0
                            # rule-range = number greater than 0, or a range of numbers greater than 0.
                        # Example : If we have thread-nb = 3 and "(1:1 ,2 ,4 -6)(2:3) " ,
                        # this means that :
                        # thread 1 verifies rules 1 ,2 ,4 ,5 ,6;
                        # thread 2 verifies only rule 3; and
                        # thread 3 verifies the rest

    output-channel = {} # Reports are sent to the output channels . The default value is a file .
            # More than one output channel is possible . In this case , the value should be a set of comma - separated values ; for example : { redis , kafka , file }.
            # Reports are sent to output channels only when the global parameters are enable ( file-output.enable = 1 ,redis-output.enable = 1 , kafka-output.enable = 1)
}
# CPU and memory usage monitor
cpu-mem-usage
{   enable = 0 # set to 1 to perform cpu-mem reporting , 0 to disable it :
    frequency = 5 # time-interval for reporting
    output-channel = {} # Reports are sent to the output channels . The default value is a file .
            # More than one output channel is possible . In this case , the value should be a set of comma - separated values ; for example : { redis , kafka , file }.
            # Reports are sent to output channels only when the global parameters are enable ( file-output.enable = 1 ,redis-output.enable = 1 , kafka-output.enable = 1)
}

#indicates behaviour analysis
behaviour
{
    enable  = 0  # set to 1 to enable , 0 to disable :
    location = "/opt/mmt/probe/result/behaviour/online/"  # Folder to write the output :
    retain-files  = 4   # retains the last x sampled files,
                                         # set to 0 to retain all files ( note that the value of retain-files must be greater than the value of thread_nb + 1) :
}

# Indicates that FTP data reconstruction should be done .
# To enable the reconstruction , also enable the options session-report.enable and session-report.ftp_report.enable
reconstruct-ftp
{
    enable = 0 # Set to 1 to enable , 0 to disable it :
    location  = "/opt/mmt/" # indicates the folder where the output file is created :
    output-channel = {} # Reports are sent to the output channels . The default value is a file .
            # More than one output channel is possible . In this case , the value should be a set of comma - separated values ; for example : { redis , kafka , file }.
            # Reports are sent to output channels only when the global parameters are enable ( file-output.enable = 1 ,redis-output.enable = 1 , kafka-output.enable = 1)
}

# socket configurations
socket
{   enable = 0 # set to 1 to enable , 0 to disable :
    domain = 0 # 0 for Unix domain , 1 for Internet domain , and 2 for both :
    socket-descriptor = "/opt/mmt/probe/bin/" # required for UNIX domain . Folder location where socket file descriptor is created :
    port = {5000} # Required for Internet domain . If one-socket-server is set to 0 then the number of port addresses should be
                  # equal to the number of threads ( thread_nb ) . If one-socket-server is set to 1 , the number of port address
                  # should be only one :
    server-address = {"localhost"} # required for Internet domain . IP address of ip_host 1 ,ip_host 2..
    one-socket-server = 1 # If set to 0 the server contains multiple sockets to receive the reports . If set to 1 only one socket will receive the reports :
}

#This report is for  host1
security-report localhost{
    enable = 0 # set to 1 to enable and 0 to disable :
    # Indicates the list of attributes that are reported when an event is triggered :
    attributes = {"ip.dst","ip.src","http.method"}
}

# This report is for security multi-session security :
security-report-multisession remote{
    enable = 0 # set to 1 to perform multi-session reporting, 0 to disable it :
    # indicates the list of attributes that are reported :
    attributes = { "nfs.file_name"}
    output-channel = {} # Reports are sent to the output channels . The default value is a file .
            # More than one output channel is possible . In this case , the value should be a set of comma - separated values ; for example : { redis , kafka , file }.
            # Reports are sent to output channels only when the global parameters are enable ( file-output.enable = 1 ,redis-output.enable = 1 , kafka-output.enable = 1)
}


# indicates the strategy for RADIUS reporting
radius-output
{
    enable = 0            # set to 1 to enable , 0 to disable :
    include-msg = 0    # indicates the message one needs to report .
                       # set to 0 to report all messages .
                       # set to a number greater than 0 to indicate the message to report (1 for message , 2 for conditions ) :
    include-condition = 0  # indicates the condition to be met in order to report a message .
                       # condition set to 1 indicates that the report should be generated iff the IP to MSISDN mapping is present .
                       # this is the only supported condition for this version . Condition set to 0 to eliminate the condition .

    output-channel = {} # Reports are sent to the output channels . The default value is a file .
            # More than one output channel is possible . In this case , the value should be a set of comma - separated values ; for example : { redis , kafka , file }.
            # Reports are sent to output channels only when the global parameters are enable ( file-output.enable = 1 ,redis-output.enable = 1 , kafka-output.enable = 1)
}

data-output
{
    include-user-agent = 32     # Indicates the threshold in terms of data volume for parsing the user agent in Web traffic.
                # The value is in kiloBytes ( kB ) . If the value is zero , this indicates that the parsing of the user agent should be done .
                # To disable the user agent parsing, set the threshold to a negative value (-1).

}

#Specifies the criteria to consider a flow as micro flow
micro-flows
{
    enable = 0                    # set to 1 to enable , 0 to disable :
    include-packet-count = 20     # packets count threshold to consider a flow as a micro flow :
    include-byte-count   = 20     # data volume threshold in KB to consider a flow as a micro flow :
    report-packet-count  = 10     # packets count threshold to report micro flows aggregated statistics :
    report-byte-count    = 10     # data volume threshold in KB to report micro flows aggregated statistics :
    report-flow-count    = 5      # Flows count threshold to report micro flows aggregated statistics :
    output-channel = {}       # Reports are sent to the output channels . The default value is a file .
            # More than one output channel is possible . In this case , the value should be a set of comma - separated values ; for example : { redis , kafka , file }.
            # Reports are sent to output channels only when the global parameters are enable ( file-output.enable = 1 ,redis-output.enable = 1 , kafka-output.enable = 1)
}

# Specifies the session timeout time in seconds for different type of applications
session-timeout
{
    default-session-timeout = 40 # 0 means default value = 60 seconds. For default session :
    long-session-timeout = 0     # 0 means default value = 600 seconds. This is reasonable for Web and SSL connections especially when long polling is used . Usually applications have a long
                                 # polling period of about 3~5 minutes .
    short-session-timeout = 0    # 0 means default value = 15 seconds . For short live sessions :
    live-session-timeout  = 0    # 0 means default value = 1500 seconds . For persistent connections like messaging applications and so on :
}

# Disable the protocol analysis
disable-proto-analysis
{
    disable-http-analysis = 0 # set to 1 to disable the http analysis and 0 to enable the http analysis:
    disable-ftp-analysis = 0 # set to 1  to disable the ftp analysis and 0 to enable the ftp analysis:
    disable-ndn-analysis = 0 # set to 1  to disable the ndn analysis and 0 to enable the ndn analysis:
    disable-ndn-http-analysis = 0 # set to 1 to disable the ndn_http analysis and 0 to ndn-http the http analysis:
    disable-radius-analysis = 0 # set to 1 to disable the radius analysis and 0 to enable the radius analysis:
    disable-rtp-analysis = 0 # set to 1 to  disable the rtp analysis and 0 to enable the rtp analysis:
}

# indicates event based reporting
event_report report1
{
    enable = 0 # set to 1 to enable , 0 to disable :
    event = "ip.src" # Indicates the event :
    attributes = {"arp.ar_hln", "ip.src"} # Indicates the list of attributes that are reported when a event is triggered :
    output-channel = {} # Reports are sent to the output channels . The default value is a file .
            # More than one output channel is possible . In this case , the value should be a set of comma - separated values ; for example : { redis , kafka , file }.
            # Reports are sent to output channels only when the global parameters are enable ( file-output.enable = 1 ,redis-output.enable = 1 , kafka-output.enable = 1)
}

# indicates session based reporting
session-report report_session
{
    #enable = 0 # set to 1 for reporting session reports , 0 to disable :
    {{session-report.enable}}
    protocols = {} # Name of the protocol, otherwise  blank. Only the session report of the protocol specified,is extracted.
    output-channel = {} # Reports are sent to the output channels . The default value is a file .
            # More than one output channel is possible . In this case , the value should be a set of comma - separated values ; for example : { redis , kafka , file }.
            # Reports are sent to output channels only when the global parameters are enable ( file-output.enable = 1 ,redis-output.enable = 1 , kafka-output.enable = 1)
}

# indicates condition based reporting and registering handlers from configuration file

condition_report report_web
{
     enable = 0        # reports are sent to output channels only when session report.enable = 1.
                       # set to 1 to enable, 0 to disable :
     condition = "WEB" # Indicates the condition to be satisfied .
     # Indicates the list of attributes for reporting .
     attributes = {"http.uri", "http.method", "http.response", "http.content_type", "http.host", "http.user_agent", "http.referer", "http.xcdn_seen", "http.content_len"}
     # Indicates the list of handlers corresponding to the above attributes .
     handlers = {"uri_handle", "http_method_handle", "http_response_handle", "mime_handle", "host_handle", "useragent_handle", "referer_handle", "xcdn_seen_handle", "content_len_handle"}
}
condition_report report_ftp
{
     enable = 0         # reports are sent to the output channels only when session-report.enable = 1.
                        # set to 1 to enable , 0 to disable :
     condition = "FTP"  # indicates the condition to be satisfied :data_transfer
     # indicates the list of attributes for reporting :
     attributes = {"ftp.data_direction", "ftp.p_payload", "ftp.packet_type", "ftp.packet_payload_len", "ftp.data_type", "ftp.file_name", "ftp.packet_request", "ftp.packet_request_parameter", "ftp.packet_response_code", "ftp.packet_reponse_value", "ftp.transfer_type", "ftp.ftp_session_mode", "ftp.file_last_modified", "ftp.session_connection_type", "ftp.user_name", "ftp.password", "ftp.last_command", "ftp.last_response_code", "ftp.file_size", "ftp.control_ip_session_id"}
     # indicates the list of handlers corresponding to the above attributes :
     handlers = {"NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "ftp_response_value_handle", "NULL", "NULL", "NULL", "ftp_session_connection_type_handle", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL"}

}
condition_report report_rtp
{
     enable = 0        # reports are sent to the output channels only when session-report.enable = 1.
                       # set to 1 to enable , 0 to disable :
     condition = "RTP" # indicates the condition to be satisfied :
     # indicates the list of attributes for reporting :
     attributes = {"rtp.version", "rtp.jitter", "rtp.loss", "rtp.order_err", "rtp.burst_loss"}
     # indicates the list of handlers corresponding to the above attributes :
     handlers = {"rtp_version_handle", "rtp_jitter_handle", "rtp_loss_handle", "rtp_order_error_handle", "rtp_burst_loss_handle"}
}
condition_report report_gtp
{
     enable = 0        # reports are sent to the output channels only when session-report.enable = 1.
                       # set to 1 to enable , 0 to disable :
     condition = "GTP" # indicates the condition to be satisfied :
     # indicates the list of attributes for reporting :
     attributes = {"gtp.teid", "ip.src" }
     # indicates the list of handlers corresponding to the above attributes :
     handlers = {"NULL", "gtp_ip_src_handle"}
}
# set reconstruct_http.enable = 1 and session-report.enable = 1 for http_reconstruction
condition_report reconstruct_http
{
     enable = 0   # Set to 1 for HTTP reconstruction, 0 to disable
     condition = "HTTP-RECONSTRUCT"      # Indicates the condition.
     location = "" # location where the files are reconstructed .
     # Indicates the list of attributes .
     attributes = {"tcp.payload_len", "tcp.p_payload", "http.msg_start", "http.header", "http.headers_end", "http.data", "http.msg_end", "http.method", "http.response","http.content_type","http.uri"}
     # Indicates the list of handlers corresponding to the above attributes .
     handlers = {"NULL", "NULL", "http_message_start_handle", "http_generic_header_handle","http_headers_end_handle","http_data_handle","http_message_end_handle","NULL","NULL","NULL","NULL","NULL"}
}

condition_report report_ssl
{
     enable = 0        # Reports are sent to output channels only when session-report.enable = 1.
                       # Set to 1 to enable , 0 to disable :
     condition = "SSL" # Indicates the condition to be satisfied :
     # Indicates the list of attributes for reporting :
     attributes = {"ssl.server_name"}
     # Indicates the list of handlers corresponding to the above attributes :
     handlers = {"ssl_server_name_handle"}
}

condition_report tcp_payload_dump
{
    enable = 0
    condition = "TCP_PAYLOAD_DUMP"
    location = "files/"
    attributes = {"ip.src", "ip.dst", "ipv6.src", "ipv6.dst", "tcp.src_port", "tcp.dest_port", "tcp.p_payload", "tcp.payload_len"}
    handlers   = {"NULL"  , "NULL"  , "NULL"    , "NULL"    , "NULL"        , "NULL"         , "NULL"         , "NULL"}
}

cache-size-for-reporting = 300000  # a value of 0 means that MMT will decide how many packets to cache ( default = 300000) :
enable-proto-without-session-stat = 0 # a value of 1 will enable and 0 will disable the protocol statics (without session):
enable-all-proto-stat = 0 # a value of 1 will enable and 0 will disable the protocol statics :
enable-IP-fragmentation-report = 0 # set to 1 to enable , 0 to disable
enable-RTT = 0  # a value of 1 to enable RTT (Round Trip Time) calculation
enable-RTT-handshake = 0  # a value of 1 to enable RTT (Round Trip Time at handshake) calculation
enable-DTT = 0  # a value of 1 to enable DTT (Data Transfer Time) calculation
#stats-period = 5 # indicates the periodicity for reports :
{{stats-period}}
#file-output-period = 5 # indicates the periodicity for reporting output file :
{{file-output-period}}
num-of-report-per-msg = 1 # indicates the number of report per message ( sockets ) .Default is 1.
                          # this option is only available for MMT-Security using sockets :
"""

import sys,os
from functools import reduce
import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

# Insert the parent path into the sys.path in order to import the mspl
parentPath = os.path.abspath("..")
if parentPath not in sys.path:
    sys.path.insert(0, parentPath)
import mspl


"""
    Output example:
    <MMT-configuration-file>
"""

class Helper:
    """
    A helper to support parsing XML attributes to MMT parameters
    """
    
    def __init__(self, obj):
        self.__obj  = obj;
        self.__list = [];
        
    def getAttr(self, name, default=None):
        """
        Same as getattr(), but allows dot notation lookup
        Discussed in: http://stackoverflow.com/questions/11975781
        """

        try:
            return reduce(getattr, name.split("."), self.__obj)
        except AttributeError as e:
            return default

    
    def __bool2int( self, val ):
        if val:
            return 1
        return 0
    
    def __updateConf( self, ident, format, val, default = "" ):
        if val == None:
            val = default

        #print(format.format(val))
        self.__list.append( (ident, format, val) )
        
    def __genExcludeMask(self, incRules ):
        """
        Generate a string representing excluded rules from a given included rules.
        Example: incRules = [1,5]
        -> Output: "2-4,6-999"
        """
        #ensure all rule ids are numbers
        incRules = list(map(int, incRules))
        #add 0 to head, 1000 to tail (when being sorted)
        incRules.append(0)
        incRules.append(1000)
        #sort inc
        incRules.sort()
        #remove duplicates
        incRules = list(dict.fromkeys(incRules))
        
        result = []
        i=1
        while i < len(incRules) :
            begin = incRules[i-1]
            end   = incRules[i]
            
            #two consecutive rule ids, e.g., 6,7
            if begin + 1 >= end:
                continue
            
            begin = begin + 1
            end   = end - 1
            #only a single rule
            if begin == end:
                result.append( begin )
            else:
                #range of rules
                result.append("{}-{}".format(begin, end))
            
            i=i+1
            
        return ",".join( map(str, result ))
    
    def appendParam(self, val, mmtAtt ):
        """
        Format parameter following MMT parameter syntax
        """
        #val = "-X{}={}".format( mmtAtt, val )
        if mmtAtt == 'probe-id':
            self.__updateConf('{{probe-id-number}}',       'probe-id-number = {}',    val, 8)
        elif mmtAtt == 'stats-period':
            self.__updateConf('{{stats-period}}',          'stats-period = {}',       val, 5)
            self.__updateConf('{{file-output-period}}',    'file-output-period = {}', val, 5)
        elif mmtAtt == 'input-source':
            self.__updateConf('{{input-source}}',          'input-source = "{}"',     val, 'ens4')
        elif mmtAtt == 'session-report.enable':
            val = self.__bool2int(val)
            self.__updateConf('{{session-report.enable}}', 'enable = {}',             val, 0)
        elif mmtAtt == 'file-output.output-dir':
            #separate filename and directory
            file = os.path.basename( val )
            dir  = os.path.dirname( val )
            
            self.__updateConf('{{file-output.data-file}}', 'data-file = "{}"',      file, 'data.csv')
            self.__updateConf('{{file-output.location}}',  'location  = "{}/"',     dir, '/opt/mmt/probe/result/report/online/')
        elif mmtAtt == 'kafka-output.hostname':
            #separate host and port. Suppose using format host:port
            val = val.split(':')
            if len(val) != 2:
                print("Kafka output format is incorrect. Mustbe host:port.")
                sys.exit(1)
            host = val[0]
            port = val[1]
            self.__updateConf('{{kafka-output.hostname}}', 'hostname = "{}"',       host, '10.79.7.178')
            self.__updateConf('{{kafka-output.port}}',     'port = {}',             port, 9092)
        elif mmtAtt == 'security.exclude-rules':
            #set default rule set to be verify
            if val == None or len(val) == 0:
                val = [77,97,99]
            val = self.__genExcludeMask( val )
            
            self.__updateConf('{{security.exclude-rules}}', 'exclude-rules = "{}"', val)
    
    def appendConfig( self, xmlAtt, mmtAtt, key = "" ):
        """
        Append a parameter to the configuration
        """

        val = self.getAttr( xmlAtt, None)
        #print("Parse {} -> {} = {}".format( xmlAtt, mmtAtt, val))
        
        
        #when the value is array
        if key != "":
            arr = val
            val = None
            for xml in arr:
                if xml.key == key:
                    val = xml.value_
                    break
        
        self.appendParam(val, mmtAtt)
        return val
    
    def getConfig( self ):
        """
        Get final configuration string
        """
        return self.__list


class ITResourceType(mspl.ITResourceType):

    def get_configuration(self):
        """Translate the ITResource element to OVS-fw configuration"""
        capability_name = self.configuration.capability[0].Name
        if not capability_name in mspl.CapabilityType.itervalues():
            raise ValueError('The capability {} does not exist, please try with the current supported capabilities'.format(capability_name))
        # In filtering case, call translation for filtering Action and filtering conf condition
        try:
            capability_configuration_function = getattr(self, "get_{}_configuration".format(capability_name))
        except AttributeError as e:
            print("Currently, {} is not implemented.".format(capability_name))
            print(e)
            sys.exit(1)

        conf = capability_configuration_function()
        # replace the values in default configuration of MMT
        global MMT_CONF
        default_config = MMT_CONF
        for (ident, format, val ) in conf:
            default_config = default_config.replace( ident, format.format(val))
        return default_config

    def get_Network_traffic_analysis_configuration(self):
        """Returns the configuration for Network_traffic_analysis capability"""
        # For filtering it is only needed the first configuration rule
        try:
            configuration_rule = self.configuration.configurationRule[0]
        except:
            print("Network_traffic_analysis capability requires at least a configurationRule element")
            sys.exit(1)
            
        action    = configuration_rule.configurationRuleAction.get_configuration()
        condition = configuration_rule.configurationCondition.get_configuration()
        extra     = configuration_rule.externalData.get_configuration()
        # Timing condition
        # TODO if neccesary
        # Application condition is not relevant on OVS-fw
        return action + condition + extra

    

class MonitoringAction(mspl.MonitoringAction):
    def get_configuration(self):
        """Returns the translated filtering MMT parameters"""
        helper = Helper( self )
        helper.appendConfig( "ProbeID", "probe-id" )
        helper.appendConfig( "reportPeriodicity", "stats-period" )
        helper.appendConfig( "reportPerFlow", "session-report.enable" )
        helper.appendConfig( "aditionalMonitoringParameters", "file-output.output-dir", "file-output" )
        helper.appendConfig( "aditionalMonitoringParameters", "kafka-output.hostname", "kafka-output" )
        return helper.getConfig()


class MonitoringConfigurationConditions(mspl.MonitoringConfigurationConditions):
    def get_configuration(self):
        """Returns the translated filtering MMT parameters"""
        
        helper = Helper( self )
        
        monitorConf = helper.getAttr("monitoringConfigurationCondition")
        
        
        sourceAddress = None
        signatureList = None
        for mon in monitorConf:
            pktFilterCondition = Helper( mon )
            #print( dir(mon.packetFilterCondition.SourceAddress) )
            sourceAddress = pktFilterCondition.getAttr("packetFilterCondition.SourceAddress", sourceAddress)
            signatureList = pktFilterCondition.getAttr("signatureList.signature", signatureList)
        
        helper.appendParam(sourceAddress, "input-source")
        helper.appendParam(signatureList, "security.exclude-rules")
        
        return helper.getConfig()


class Priority( mspl.Priority ):
    def get_configuration(self):
        #print("Get configuration of priority")
        return []

class M2LPlugin:
    """MMT Medium to low plugin implementation"""

    def get_configuration(self, mspl_source):
        """Return the MMT configuration from the mspl_source"""
        # Customize the pyxb generated classes with our own behavior
        global mspl
        mspl.ITResourceType._SetSupersedingClass(ITResourceType)
        mspl.MonitoringAction._SetSupersedingClass(MonitoringAction)
        mspl.Priority._SetSupersedingClass(Priority)
        mspl.MonitoringConfigurationConditions._SetSupersedingClass(MonitoringConfigurationConditions)
        
        # Load the xml
        it_resource = mspl.CreateFromDocument(mspl_source)
        # Start the parser process using the xml root element
        # If the type is policy for orchestration we get the mspl inside
        if isinstance(it_resource,mspl.raw.mspl.ITResourceOrchestrationType):
            it_resource = it_resource.ITResource[0]
            
        return it_resource.get_configuration()


if __name__ == "__main__":
    if len(sys.argv ) < 2:
        print("Usage:{} xmlInputFile [outputFile]".format( sys.argv[0]))
        print("\t-xmlInputFile (required): file containing xml to be translated")
        print("\t-outputFile   (optional): file to contain output. If ignore, output to the screen.")
        print("")
        sys.exit(1)
        
    xml_file    = sys.argv[1]

    output_file = None
    if len(sys.argv ) > 2:
        output_file = sys.argv[2]
    
    # Instantiate the plugin
    m2lplugin = M2LPlugin()
    xml_source = open(xml_file).read()
    enabler_configuration = m2lplugin.get_configuration(xml_source)
    if output_file == None:
        print(  enabler_configuration )
    else:
        f = open( output_file, 'w+' )
        f.write( enabler_configuration )
        f.close()
        print("Write mmt-configuration to {}".format( output_file ))
