# # -*- coding: utf-8 -*-
"""
This python module implements a MSPL->Ericsson platform Northbound API plugin inside the ANASTACIA European Project,
extending the MSPL language defined in secured project.
How to use:
	python3 mspl_odl.py [MSPL_FILE.xml]

"""
__author__ = "Alejandro Molina Zarca"
__copyright__ = "Copyright 2018, ANASTACIA H2020"
__credits__ = ["Antonio Skarmeta", "Jorge Bernal Bernabé", "Alejandro Molina Zarca"]
__license__ = "GPL"
__version__ = "0.0.1"
__maintainer__ = "Alejandro Molina Zarca"
__email__ = "alejandro.mzarca@um.es"
__status__ = "Development"


import sys,os
import json
# Insert the parent path into the sys.path in order to import the mspl
parentPath = os.path.abspath("..")
if parentPath not in sys.path:
    sys.path.insert(0, parentPath)
import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)
import mspl

"""
	Network Slicing output example:

	{
	  "action": TRANSFER,
	  "source": [MAC_ADDR],
	  "destination": "Slice2"
	  "priority": 6000
	}


"""

class ITResourceType(mspl.ITResourceType):


	def get_configuration(self):
		'Translate the ITResource element to Ericsson Northbound API configuration'
		capability_name = self.configuration.capability[0].Name
		if not capability_name in mspl.CapabilityType.itervalues():
			raise ValueError('The capability {} does not exist, please try with the current supported capabilities'.format(capability_name))
		# In filtering case, call translation for filtering Action and filtering conf condition
		try:
			capability_configuration_function = getattr(self, "get_{}_configuration".format(capability_name))
		except AttributeError as e:
			print("Currently, {} is not implemented.".format(capability_name))
			print(e)
			sys.exit(1)

		enabler_configuration = capability_configuration_function()
		return enabler_configuration


	def get_Network_slicing_configuration(self):
		try:
			configuration_rule = self.configuration.configurationRule[0]
		except:
			print("Network Slicing capability requires at least a configurationRule element")
			sys.exit(1)
		action = configuration_rule.configurationRuleAction.get_configuration()
		condition = configuration_rule.configurationCondition.get_configuration()
		priority = configuration_rule.externalData.get_configuration()

		enabler_configuration = {**action,**condition,'priority':priority}

		return enabler_configuration


class NetworkSlicingAction(mspl.NetworkSlicingAction):
	def get_configuration(self):
		'Returns the Slicing action for Ericsson Northbound API'
		action = self.NetworkSlicingActionType
		parameters = self.NetworkSlicingActionParameters
		# TODO: Translate the action and paramters to your HTTP API parameters
		parsed_parameters = {}
		for parameter in parameters.NetworkSlicingActionParameter:
			parsed_parameters[parameter.key] = parameter.value_
		return {'action':action,'parameters':parsed_parameters}
		

class Priority(mspl.Priority):
	def get_configuration(self):
		'Returns the priority of the configuration rule'
		return self.value_ if self.value_ else 0


class NetworkSlicingConfigurationCondition(mspl.NetworkSlicingConfigurationCondition):
	def get_configuration(self):
		'Returns the traslated conditions for Ericsson Northbound API'
		filtering_enabler_conf = []
		packet_filter_condition = self.packetFilterCondition
		if not packet_filter_condition:
			return None
		return {'source':packet_filter_condition.Interface}



class M2LPlugin:
	'Ericsson Northbound API Medium to low plugin implementation'

	def get_configuration(self,mspl_source):
		'Return Ericsson Northbound API configuration from the mspl_source'
		# Customize the pyxb generated classes with our own behavior
		
		global mspl
		mspl.ITResourceType._SetSupersedingClass(ITResourceType)
		mspl.NetworkSlicingAction._SetSupersedingClass(NetworkSlicingAction)
		mspl.Priority._SetSupersedingClass(Priority)
		mspl.NetworkSlicingConfigurationCondition._SetSupersedingClass(NetworkSlicingConfigurationCondition)
		#Load the xml
		it_resource = mspl.CreateFromDocument(mspl_source)
		# Start the parser process using the xml root element
		# If the type is policy for orchestration we get the mspl inside
		if isinstance(it_resource,mspl.raw.mspl.ITResourceOrchestrationType):
			it_resource = it_resource.ITResource[0]

		return it_resource.get_configuration()

		
if __name__ == "__main__":
	xml_file = sys.argv[1] 
	m2lplugin = M2LPlugin()
	print("Reading mspl file...")
	xml_source = open(xml_file).read()
	enabler_configuration = m2lplugin.get_configuration(xml_source)
	# Pretty print
	separator = "*"
	separator_group = separator*5
	title = "{} CONFIGURATION".format(sys.argv[0].split("mspl_")[1][:-3].upper())
	print("\n{}{}{}".format(separator_group,title,separator_group))
	print(enabler_configuration)
	print("{}{}{}\n".format(separator_group,separator*len(title),separator_group))