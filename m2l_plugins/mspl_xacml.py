# # -*- coding: utf-8 -*-
"""
This python module implements a MSPL->IPTABLES plugin inside the ANASTACIA European Project,
extending the MSPL language defined in secured project.
How to use:
	python3 mspl_iptables.py [MSPL_FILE.xml]

"""
__author__ = "Alejandro Molina Zarca"
__copyright__ = "Copyright 2018, ANASTACIA H2020"
__credits__ = ["Antonio Skarmeta", "Jorge Bernal Bernabé", "Alejandro Molina Zarca"]
__license__ = "GPL"
__version__ = "0.0.1"
__maintainer__ = "Alejandro Molina Zarca"
__email__ = "alejandro.mzarca@um.es"
__status__ = "Development"


import sys,os
import json
import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)
# Insert the parent path into the sys.path in order to import the mspl
parentPath = os.path.abspath("..")
if parentPath not in sys.path:
    sys.path.insert(0, parentPath)
import mspl

from string import Template

"""
	Output example:
	{"output":{"psa_config":"*filter\n:INPUT ACCEPT [0:0]\n:OUTPUT ACCEPT [0:0]\n:FORWARD ACCEPT [0:0]\n-A FORWARD -p TCP -m time --timestart 08:00 --timestop 19:00 -j DROP\n-A FORWARD -p UDP -m time --timestart 08:00 --timestop 19:00 -j DROP\nCOMMIT\n"}}
"""

class ITResourceType(mspl.ITResourceType):

	def get_configuration(self):
		'Translate the ITResource element to IPTABLES configuration'
		print ("get_configuration")
		capability_name = self.configuration.capability[0].Name
		if not capability_name in mspl.CapabilityType.itervalues():
			raise ValueError('The capability {} does not exist, please try with the current supported capabilities'.format(capability_name))
		# In filtering case, call translation for filtering Action and filtering conf condition
		try:
			print("getting get_{}_configuration".format(capability_name))
			capability_configuration_function = getattr(self, "get_{}_configuration".format(capability_name))
		except AttributeError as e:
			print("Currently, {} is not implemented.".format(capability_name))
			print(e)
			sys.exit(1)

		rs = capability_configuration_function()
		
		return rs

	def get_AuthoriseAccess_resurce_configuration(self):
		'Returns the configuration for AuthoriseAccess_resurce capability'
		try:
			# 
			# Loading ITResource > configuration > configurationRule
			# configuration_rule <-  (ITResource > configuration > configurationRule)

			configuration_rule = self.configuration.configurationRule[0]

		except:
			print("AuthoriseAccess_resurce capability requires at least a configurationRule element")
			sys.exit(1)


		# Loading configurationRule > configurationRuleAction > technologyActionParameters > additionalNetworkConfigurationParameters
		# Binded Class -> RemoteAccessNetworkConfiguration
		AuthorizationActionType, AuthorizationSubject, AuthorizationTarget = configuration_rule.configurationRuleAction.get_configuration()
		URL, method = configuration_rule.configurationCondition.applicationLayerCondition.get_configuration()
		sourceAddress, destinationAddress = configuration_rule.configurationCondition.get_configuration()

		if(AuthorizationActionType == "ALLOW"):
			AuthorizationActionType = "Permit"


		jsonTemplate = """<?xml version="1.0" encoding="UTF-8"?>
<PolicySet xmlns="urn:oasis:names:tc:xacml:2.0:policy:schema:os" PolicyCombiningAlgId="urn:oasis:names:tc:xacml:1.0:policy-combining-algorithm:first-applicable" PolicySetId="POLICY_SET">
  <Target />
  <Policy PolicyId="bootstrapping" RuleCombiningAlgId="urn:oasis:names:tc:xacml:1.0:rule-combining-algorithm:first-applicable">
    <Target />
    <Rule Effect="$AuthorizationActionType" RuleId="secure_bootstrapping">
      <Target>
        <Subjects>
          <Subject>
            <SubjectMatch MatchId="urn:oasis:names:tc:xacml:1.0:function:string-equal">
              <AttributeValue DataType="http://www.w3.org/2001/XMLSchema#string">$sourceAddress</AttributeValue>
              <SubjectAttributeDesignator AttributeId="urn:oasis:names:tc:xacml:2.0:subject:role" DataType="http://www.w3.org/2001/XMLSchema#string" />
            </SubjectMatch>
          </Subject>
        </Subjects>
        <Resources>
          <Resource>
            <ResourceMatch MatchId="urn:oasis:names:tc:xacml:1.0:function:string-equal">
              <AttributeValue DataType="http://www.w3.org/2001/XMLSchema#string">$destinationAddress$URL</AttributeValue>
              <ResourceAttributeDesignator AttributeId="urn:oasis:names:tc:xacml:1.0:resource:resource-id" DataType="http://www.w3.org/2001/XMLSchema#string" />
            </ResourceMatch>
          </Resource>
        </Resources>
        <Actions>
          <Action>
            <ActionMatch MatchId="urn:oasis:names:tc:xacml:1.0:function:string-equal">
              <AttributeValue DataType="http://www.w3.org/2001/XMLSchema#string">$method</AttributeValue>
              <ActionAttributeDesignator AttributeId="urn:oasis:names:tc:xacml:1.0:action:action-id" DataType="http://www.w3.org/2001/XMLSchema#string" />
            </ActionMatch>
          </Action>
        </Actions>
      </Target>
    </Rule>
  </Policy>
</PolicySet>
		"""

		t = Template(jsonTemplate);
		result = t.substitute ( {
			'AuthorizationActionType': AuthorizationActionType ,
			'URL': URL ,
			'method': method ,
			'sourceAddress': sourceAddress ,
			'destinationAddress': destinationAddress
		} )

		print(result)


		return result



class AuthorizationAction(mspl.AuthorizationAction):
	def get_configuration(self):


		AuthorizationActionType = self.AuthorizationActionType
		AuthorizationSubject = self.AuthorizationSubject
		AuthorizationTarget = self.AuthorizationTarget

		print ("AuthorizationActionType: ", AuthorizationActionType)
		print ("AuthorizationSubject: ", AuthorizationSubject)
		print ("AuthorizationTarget: ", AuthorizationTarget)

		return (AuthorizationActionType, AuthorizationSubject, AuthorizationTarget )


class AuthorizationCondition(mspl.AuthorizationCondition):
	def get_configuration(self):


		isCNF = self.isCNF
		sourceAddress = self.packetFilterCondition.SourceAddress
		destinationAddress = self.packetFilterCondition.DestinationAddress


		print ("isCNF: ", isCNF)
		print ("sourceAddress: ", sourceAddress)
		print ("destinationAddress: ", destinationAddress)

		return (sourceAddress, destinationAddress)


class IoTApplicationLayerCondition(mspl.IoTApplicationLayerCondition):
	def get_configuration(self):

		URL = self.URL
		method = self.method

		print ("URL: ", URL)
		print ("method: ", method)

		return (URL, method)





class M2LPlugin:
	'IPTABLES Medium to low plugin implementation'

	def get_configuration(self,mspl_source):
		'Return the IPTABLES configuration from the mspl_source'
		# Customize the pyxb generated classes with our own behavior
		global mspl
		mspl.ITResourceType._SetSupersedingClass(ITResourceType)
		mspl.AuthorizationAction._SetSupersedingClass(AuthorizationAction)
		mspl.IoTApplicationLayerCondition._SetSupersedingClass(IoTApplicationLayerCondition)
		mspl.AuthorizationCondition._SetSupersedingClass(AuthorizationCondition)



		#Load the xml
		it_resource = mspl.CreateFromDocument(mspl_source)
		# Start the parser process using the xml root element
		return it_resource.get_configuration()

if __name__ == "__main__":
	xml_file = sys.argv[1] 
	# Instantiate the plugin
	m2lplugin = M2LPlugin()
	logger.info("Reading mspl file...")
	xml_source = open(xml_file).read()
	logger.info("Translating mspl to IPTABLES...")
	enabler_configuration = m2lplugin.get_configuration(xml_source)

	print ("Result")
	print (enabler_configuration)
