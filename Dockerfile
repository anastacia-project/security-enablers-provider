# Use an official Python runtime as a parent image
FROM python:3

# Set the working directory to /app
WORKDIR /app

# Copy the current directory contents into the container at /app
ADD . /app

# Install any needed packages specified in requirements.txt
RUN pip install --trusted-host pypi.python.org -r requirements.txt

# Make port 80 available to the world outside this container
EXPOSE 8001

# Define environment variable
#ENV NAME World

# Run app.py when the container launches
#CMD ["gunicorn", "-b", "0.0.0.0:8001", "--reload", "security_enabler_provider:app"] 
CMD ["gunicorn", "-b", "0.0.0.0:8001", "security_enabler_provider:app"] 